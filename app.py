# This file is part of the Toolhub gadget demo application.
#
# Copyright (C) 2021 Bryan Davis and contributors
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging.config
import os

from authlib.integrations.flask_client import OAuth
import flask
import toolforge
import yaml


__dir__ = os.path.dirname(__file__)


logging.config.dictConfig({
    "version": 1,
    "formatters": {
        "default": {
            "format": "%(asctime)s %(module)s %(levelname)s: %(message)s",
            "datefmt": "%Y-%m-%dT%H:%M:%SZ",
        },
    },
    "handlers": {
        "wsgi": {
            "class": "logging.StreamHandler",
            "stream": "ext://flask.logging.wsgi_errors_stream",
            "formatter": "default",
        },
    },
    "root": {
        "level": "INFO",
        "handlers": ["wsgi"],
    },
})
app = flask.Flask(__name__)
app.config.update(yaml.safe_load(open(os.path.join(__dir__, "config.yaml"))))


toolforge.set_user_agent("toolhub-gadget-demo")


oauth = OAuth(app)
oauth.register(
    name="toolhub",
    access_token_url="https://toolhub-demo.wmcloud.org/o/token/",
    access_token_params=None,
    authorize_url="https://toolhub-demo.wmcloud.org/o/authorize/",
    authorize_params=None,
    api_base_url="https://toolhub-demo.wmcloud.org/api/",
    client_kwargs=None,
)


@app.route("/")
def index():
    ctx = {
        "profile": flask.session.get("profile"),
    }
    return flask.render_template("home.html", **ctx)


@app.route("/login")
def login():
    redirect_uri = flask.url_for("authorize", _external=True)
    return oauth.toolhub.authorize_redirect(redirect_uri)


@app.route("/authorize")
def authorize():
    token = oauth.toolhub.authorize_access_token()
    flask.session["token"] = token
    resp = oauth.toolhub.get("user/")
    resp.raise_for_status()
    flask.session["profile"] = resp.json()
    app.logger.info("profile: %s", flask.session["profile"])
    return flask.redirect("/")


@app.route("/logout")
def logout():
    flask.session.pop("token", None)
    flask.session.pop("profile", None)
    return flask.redirect("/")
